import gramfuzz

fuzzer = gramfuzz.GramFuzzer()
fuzzer.load_grammar("SeQreL_grammar.py")
queries = fuzzer.gen(cat="query", num=10)
print(str.encode("\n").join(queries).decode())
